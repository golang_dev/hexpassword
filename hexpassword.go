package hexpassword

import (
	"encoding/hex"
	"github.com/seehuhn/password"
)

var Version = "v0.0.5-4"

func Read(prompt string) (string){
	bpass, _ := password.Read(prompt) 
	return string(bpass)
}

func ReadAndEncode(prompt string, SecID string) (string){
	bpass, _ := password.Read(prompt) 
	bpass = []byte(SecID + string(bpass))
	hexpass  := hex.EncodeToString(bpass)
	return hexpass
}

func Decode(Password string) (string){
	dehexpass,_ := hex.DecodeString(Password)
	return string(dehexpass)
}

func Encode(Password string) (string){
	hexpass  := hex.EncodeToString([]byte(Password))
	return hexpass
}
